import React, {useContext} from 'react';
import {MainContext} from "../main/MainStore.Context";

const Home = () => {
    const mainContext = useContext(MainContext);

    return (
        <div>
            home page
            <br/>
            <h1>{mainContext.token}</h1>
        </div>
    );
};

export default Home;
