import React, {useContext} from 'react';
import {NavLink} from "react-router-dom";
import {MainContext} from "../../main/MainStore.Context";

const Header = () => {
    const mainContext = useContext(MainContext);
    return (
        <div>
            <NavLink to={'/login'}>login page</NavLink> |
            <NavLink to={'/'}>home page</NavLink>
            <hr />
            {
                mainContext.token ? mainContext.token : 'token not set'
            }
            <hr/>
            <br/>

        </div>
    );
};

export default Header;
