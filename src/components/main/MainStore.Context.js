import React, {useState} from 'react';

const initialData = {
    token: null
}

export const MainContext = React.createContext({...initialData});

const MainStore = (props) => {

    const [token, setToken] = useState(initialData.token);

    return (
        <MainContext.Provider value={{token, setToken}}>
            {props.children}
        </MainContext.Provider>
    );
};

export default MainStore;
