import React,{useContext} from 'react';
import {MainContext} from "../main/MainStore.Context";

const Login = () => {
    const mainContext = useContext(MainContext);
    return (
        <div>
            <h1>login page</h1>
            <h2>{mainContext.token}</h2>
            <button onClick={()=>mainContext.setToken('new token')}>change token</button>
            <button onClick={()=>mainContext.setToken(null)}>remove token</button>
        </div>
    );
};

export default Login;
