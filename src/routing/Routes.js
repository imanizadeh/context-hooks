import React,{useContext} from 'react';
import {Router,Switch,Route} from 'react-router-dom';
import History from "./History";
import Home from "../components/home/Home.Content";
import Header from "../components/shared/header/Header.Content";
import Login from "../components/login/Login.Content";
import {MainContext} from "../components/main/MainStore.Context";

const Routes = () => {
    const mainContext = useContext(MainContext);
    return (
        <Router history={History}>
            {mainContext.token && <Header/>}
            <Switch>
                <Route path={'/'} exact render={()=> <Home />} />
                <Route path={'/login'} exact render={()=> <Login />} />
            </Switch>
        </Router>
    );
};

export default Routes;
